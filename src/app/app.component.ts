import { Categoria } from './../shared/models/categorias.model';
import { CategoriasService } from './../shared/services/categorias.service';
import { Component, OnInit } from '@angular/core';
import { LancamentosService } from '../shared/services/lancamentos.service';
import { Observable } from 'rxjs';
import { HttpResponse } from '@angular/common/http';
import { Lancamentos } from '../shared/models/lancamentos.model';
import { Mes } from 'src/shared/models/mes.model';
import { IMes } from '../shared/models/mes.model';
import { UtilService } from 'src/shared/services/util.service';
import { Consolidados } from 'src/shared/models/consolidado.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'desafio3';

  constructor() { }

}
