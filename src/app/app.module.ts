import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { registerLocaleData } from '@angular/common';
import localePt from '@angular/common/locales/pt';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material.module';
import { ListaCategoriaModule } from './lista-categoria/lista-categoria.module';
import { ListaMesComponent } from './lista-mes/lista-mes.component';
import { ListaMesModule } from './lista-mes/lista-mes.module';
import { ListaGeralModule } from './lista-geral/lista-geral.module';


registerLocaleData(localePt);

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MaterialModule,
    ListaCategoriaModule,
    ListaMesModule,
    ListaGeralModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {

}
