import { OnInit, Component } from '@angular/core';
import { Categoria } from 'src/shared/models/categorias.model';
import { Lancamentos } from 'src/shared/models/lancamentos.model';
import { Consolidados } from 'src/shared/models/consolidado.model';
import { CategoriasService } from 'src/shared/services/categorias.service';
import { LancamentosService } from 'src/shared/services/lancamentos.service';

@Component({
  selector: 'app-lista-categoria',
  templateUrl: './lista-categoria.component.html',
  styleUrls: ['./lista-categoria.component.css']
})
export class ListaCategoriaComponent implements OnInit {
  listCategory: Categoria[];
  listReleases: Lancamentos[];
  listConsolidatedCategory: Consolidados[] = [];
  displayedColumnsCategory: string[] = ['descricao', 'valor'];
  dataSourceConsolidatedCategory;

  constructor(private categoriasService: CategoriasService, private lancamentosService: LancamentosService) { }

  ngOnInit(): void {
    this.categoriasService.getAllCategory().subscribe(categories => {
      this.listCategory = categories.body;

      this.lancamentosService.getAllReleases().subscribe(releases => {
        this.listReleases = releases.body;
        this.getConsolidatedCategory();
      });
    });
  }

  getConsolidatedCategory(): void {
    this.listCategory.forEach(item => {

      const listValues = this.listReleases.filter(x => x.categoria === item.id);

      let value = 0;
      listValues.forEach(release => value += release.valor);

      if (value > 0) {
        this.listConsolidatedCategory.push(new Consolidados(value, item.nome));
      }
      this.dataSourceConsolidatedCategory = this.listConsolidatedCategory;
    });

  }
}
