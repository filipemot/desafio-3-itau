import { OnInit, Component } from '@angular/core';
import { Lancamentos } from 'src/shared/models/lancamentos.model';
import { Consolidados } from 'src/shared/models/consolidado.model';
import { LancamentosService } from 'src/shared/services/lancamentos.service';
import { UtilService } from '../../shared/services/util.service';
import { Categoria } from 'src/shared/models/categorias.model';
import { CategoriasService } from 'src/shared/services/categorias.service';

@Component({
  selector: 'app-lista-geral',
  templateUrl: './lista-geral.component.html',
  styleUrls: ['./lista-geral.component.css']
})
export class ListaGeralComponent implements OnInit {

  listCategory: Categoria[];
  listReleases: Lancamentos[];

  listConsolidated: Consolidados[] = [];
  displayedColumnsReleases: string[] = ['valor', 'origem', 'categoria', 'mes_lancamento'];
  dataSourceReleases;

  constructor(private utilService: UtilService,
              private lancamentosService: LancamentosService,
              private categoriasService: CategoriasService) { }

  ngOnInit(): void {
    this.categoriasService.getAllCategory().subscribe(categories => {
      this.listCategory = categories.body;

      this.lancamentosService.getAllReleases().subscribe(releases => {
        this.listReleases = releases.body;
        this.dataSourceReleases = releases.body;
      });
    });
  }

  getMonth(id: number): string {
    return this.utilService.getMonth(id);
  }

  getCategory(id: number): string {
    const category = this.listCategory.find(x => x.id === id);

    if (category) {
      return category.nome;
    }

    return '';
  }
}
