import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { HttpClientModule } from '@angular/common/http';
import { registerLocaleData } from '@angular/common';
import localePt from '@angular/common/locales/pt';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MaterialModule } from '../material.module';
import { ListaGeralComponent } from './lista-geral.component';


registerLocaleData(localePt);

@NgModule({
  declarations: [
    ListaGeralComponent
  ],
  exports: [
    ListaGeralComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MaterialModule
  ],
  providers: []
})
export class ListaGeralModule {}
