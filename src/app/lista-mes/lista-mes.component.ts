import { OnInit, Component } from '@angular/core';
import { Lancamentos } from 'src/shared/models/lancamentos.model';
import { Consolidados } from 'src/shared/models/consolidado.model';
import { LancamentosService } from 'src/shared/services/lancamentos.service';
import { UtilService } from '../../shared/services/util.service';

@Component({
  selector: 'app-lista-mes',
  templateUrl: './lista-mes.component.html',
  styleUrls: ['./lista-mes.component.css']
})
export class ListaMesComponent implements OnInit {
  listReleases: Lancamentos[];
  listConsolidated: Consolidados[] = [];
  displayedColumns: string[] = ['descricao', 'valor'];
  dataSourceConsolidated;

  constructor(private utilService: UtilService, private lancamentosService: LancamentosService) { }

  ngOnInit(): void {
      this.lancamentosService.getAllReleases().subscribe(releases => {
        this.listReleases = releases.body;
        this.getConsolidatedMonth();
      });
  }

  getConsolidatedMonth(): void {
    this.utilService.getMonths().forEach(item => {

      const listValues = this.listReleases.filter(x => x.mes_lancamento === item.id);

      let value = 0;
      listValues.forEach(release => value += release.valor);

      if (value > 0) {
        this.listConsolidated.push(new Consolidados(value, item.name));
      }
      this.dataSourceConsolidated = this.listConsolidated;
    });

  }
}
