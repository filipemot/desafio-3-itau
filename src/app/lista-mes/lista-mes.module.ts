import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { HttpClientModule } from '@angular/common/http';
import { registerLocaleData } from '@angular/common';
import localePt from '@angular/common/locales/pt';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ListaMesComponent } from './lista-mes.component';
import { MaterialModule } from '../material.module';


registerLocaleData(localePt);

@NgModule({
  declarations: [
    ListaMesComponent
  ],
  exports: [
    ListaMesComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MaterialModule
  ],
  providers: []
})
export class ListaMesModule {

}
