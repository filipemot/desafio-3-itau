export interface IConsolidados {
  valor?: number;
  descricao?: string;
}

export class Consolidados implements IConsolidados {
  constructor(
    public valor?: number,
    public descricao?: string
  ) { }
}
