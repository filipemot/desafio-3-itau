export interface ILancamentos {
  id?: number;
  valor?: number;
  categoria?: number;
  origem?: string;
  mes_lancamento?: number;
}

export class Lancamentos implements ILancamentos {
  constructor(
    public id?: number,
    public valor?: number,
    public categoria?: number,
    public origem?: string,
    public mes_lancamento?: number
  ) { }
}
