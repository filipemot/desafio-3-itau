export interface IMes {
  id?: number;
  name?: string;
}

export class Mes implements IMes {
  constructor(
    public id?: number,
    public name?: string,
  ) { }
}
