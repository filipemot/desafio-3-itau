import { ICategoria } from './../models/categorias.model';
import { Injectable } from '@angular/core';
import { SERVER_API_URL } from 'src/app/app.constants';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

type EntityArrayResponseType = HttpResponse<ICategoria[]>;

@Injectable({ providedIn: 'root' })
export class CategoriasService {
  public resourceUrl = SERVER_API_URL + 'categorias';

  constructor(protected http: HttpClient) { }

  getAllCategory(): Observable<EntityArrayResponseType> {
    return this.http.get<ICategoria[]>(this.resourceUrl, { observe: 'response' });
  }
}
