import { ICategoria } from './../models/categorias.model';
import { Injectable } from '@angular/core';
import { SERVER_API_URL } from 'src/app/app.constants';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ILancamentos } from '../models/lancamentos.model';

type EntityArrayResponseType = HttpResponse<ILancamentos[]>;

@Injectable({ providedIn: 'root' })
export class LancamentosService {
  public resourceUrl = SERVER_API_URL + 'lancamentos';

  constructor(protected http: HttpClient) { }

  getAllReleases(): Observable<EntityArrayResponseType> {
    return this.http.get<ILancamentos[]>(this.resourceUrl, { observe: 'response' });
  }
}
